package com.meli.xmen;

public class Genetics {
  private Node dna;
  private int coincidences = 0;

  public Genetics(String[][] arr) {
    dna = build(arr, arr.length - 1, 0, 0);
    laboratoryStudy(dna);
  }

  public boolean isMutant() {
    return (this.coincidences > 3);
  }

  private Node build(String[][] arr, int baseMatrix, int i, int j) {
    if (baseMatrix <= i - 1 || baseMatrix <= j - 1) return null;
    Node n = new Node();
    n.setData(arr[i][j].charAt(0));
    n.setRightNode(build(arr, baseMatrix, i, j + 1));
    n.setNodeBelow(build(arr, baseMatrix, i + 1, j));
    n.setNodeDiagonalRight(build(arr, baseMatrix, i + 1, j + 1));
    if (j > 0) n.setNodeDiagonalLeft(build(arr, baseMatrix, i + 1, j - 1));
    return n;
  }

  private void laboratoryStudy(Node n) {

    Node currentNode = n;
    Node rightNode;

    init:
    while (currentNode != null) {
      rightNode = currentNode;
      while (rightNode != null) {
        this.coincidences += rightNode.isEquals(rightNode.getRightNode(), 0, "right");
        if (this.coincidences >= 6) break init;

        this.coincidences += rightNode.isEquals(rightNode.getNodeDiagonalRight(), 0, "diagonalRight");
        if (this.coincidences >= 6) break init;

        this.coincidences += rightNode.isEquals(rightNode.getNodeBelow(), 0, "down");
        if (this.coincidences >= 6) break init;

        this.coincidences += rightNode.isEquals(rightNode.getNodeDiagonalLeft(), 0, "diagonalLeft");
        if (this.coincidences >= 6) break init;

        rightNode = rightNode.getRightNode();
      }
      currentNode = currentNode.getNodeBelow();
    }
  }

  public void nodes() {
    Node currentNode = this.dna;
    Node rightNode;

    while (currentNode != null) {
      rightNode = currentNode;
      while (rightNode != null) {
        String right = (rightNode.getRightNode() != null) ? String.valueOf(rightNode.getRightNode().getData()) : "N";
        String obliqueRight = (rightNode.getNodeDiagonalRight() != null) ? String.valueOf(rightNode.getNodeDiagonalRight().getData()) : "N";
        String down = (rightNode.getNodeBelow() != null) ? String.valueOf(rightNode.getNodeBelow().getData()) : "N";
        String obliqueLeft = (rightNode.getNodeDiagonalLeft() != null) ? String.valueOf(rightNode.getNodeDiagonalLeft().getData()) : "N";
        System.out.print("   " + rightNode.getData() + " - " + right + "\n");
        System.out.println(" / | \\");
        System.out.print(obliqueLeft + "  " + down + "  " + obliqueRight + "\n");
        rightNode = rightNode.getRightNode();
        System.out.println();
      }
      currentNode = currentNode.getNodeBelow();
    }
  }

  public void display() {
    Node currentNode = this.dna;
    Node rightNode;

    while (currentNode != null) {
      rightNode = currentNode;
      while (rightNode != null) {
        System.out.print(rightNode.getData() + " ");
        rightNode = rightNode.getRightNode();
      }
      System.out.println();
      currentNode = currentNode.getNodeBelow();
    }
  }
}
