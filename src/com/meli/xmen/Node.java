package com.meli.xmen;

public class Node {
  char data;
  Node rightNode;
  Node nodeDiagonalRight;
  Node nodeBelow;
  Node nodeDiagonalLeft;

  public char getData() {
    return data;
  }

  public void setData(char data) {
    this.data = data;
  }

  public Node getRightNode() {
    return rightNode;
  }

  public void setRightNode(Node rightNode) {
    this.rightNode = rightNode;
  }

  public Node getNodeDiagonalRight() {
    return nodeDiagonalRight;
  }

  public void setNodeDiagonalRight(Node nodeDiagonalRight) {
    this.nodeDiagonalRight = nodeDiagonalRight;
  }

  public Node getNodeBelow() {
    return nodeBelow;
  }

  public void setNodeBelow(Node nodeBelow) {
    this.nodeBelow = nodeBelow;
  }

  public Node getNodeDiagonalLeft() {
    return nodeDiagonalLeft;
  }

  public void setNodeDiagonalLeft(Node nodeDiagonalLeft) {
    this.nodeDiagonalLeft = nodeDiagonalLeft;
  }

  @Override
  public String toString() {
    return String.format(
            "data: %c \n rightNode: %c \n rightObliqueNode: %c \n nodeBelow: %c \n leftObliqueNode: %c",
            this.data,
            (this.rightNode != null) ? this.rightNode.getData() : 'n',
            (this.nodeDiagonalRight != null) ? this.nodeDiagonalRight.getData() : 'n',
            (this.nodeBelow != null) ? this.nodeBelow.getData() : 'n',
            (this.nodeDiagonalLeft != null) ? this.nodeDiagonalLeft.getData() : 'n'
    );
  }

  public int isEquals(Node node, int level, String direction) {
    if (level == 3 || node == null) return level;

    if (this.data == node.getData()) {
      switch (direction) {
        case "right":
          if (node.getRightNode() != null || level == 2)
            return node.isEquals(node.getRightNode(), level + 1, direction);
          break;
        case "diagonalRight":
          if (node.getNodeDiagonalRight() != null || level == 2)
            return node.isEquals(node.getNodeDiagonalRight(), level + 1, direction);
          break;
        case "down":
          if (node.getNodeBelow() != null || level == 2)
            return node.isEquals(node.getNodeBelow(), level + 1, direction);
          break;
        case "diagonalLeft":
          if (node.getNodeDiagonalLeft() != null || level == 2)
            return node.isEquals(node.getNodeDiagonalLeft(), level + 1, direction);
          break;
      }
    }
    return 0;
  }
}
